package com.tsc.skuschenko.tm.command.data;

import com.tsc.skuschenko.tm.dto.Domain;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.EmptyBinaryPathException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Optional;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "load data from xml file";

    @NotNull
    private static final String METHOD_TYPE = "jaxb";

    @NotNull
    private static final String NAME = "data-load-xml-jaxb";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileXmlPath(METHOD_TYPE);
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyBinaryPathException::new);
        @NotNull JAXBContext jaxbContext =
                JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller =
                jaxbContext.createUnmarshaller();
        @NotNull final File file = new File(filePath);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

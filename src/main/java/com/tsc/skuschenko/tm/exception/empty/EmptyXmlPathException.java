package com.tsc.skuschenko.tm.exception.empty;

import com.tsc.skuschenko.tm.exception.AbstractException;

public class EmptyXmlPathException extends AbstractException {

    public EmptyXmlPathException() {
        super("Error! path to file xml is empty...");
    }

}

package com.tsc.skuschenko.tm.exception.empty;

import com.tsc.skuschenko.tm.exception.AbstractException;

public class EmptyJsonPathException extends AbstractException {

    public EmptyJsonPathException() {
        super("Error! path to file json is empty...");
    }

}

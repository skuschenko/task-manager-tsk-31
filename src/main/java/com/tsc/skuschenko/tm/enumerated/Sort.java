package com.tsc.skuschenko.tm.enumerated;

import com.tsc.skuschenko.tm.comparator.ComparatorByCreated;
import com.tsc.skuschenko.tm.comparator.ComparatorByDateStart;
import com.tsc.skuschenko.tm.comparator.ComparatorByName;
import com.tsc.skuschenko.tm.comparator.ComparatorByStatus;
import lombok.Getter;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

@Getter
public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by start date", ComparatorByDateStart.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    @Nullable
    private final Comparator comparator;
    @Nullable
    private final String displayName;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
